package com.self.dao;

import com.self.pojo.Article;
import org.springframework.stereotype.Repository;

/**
 * Created by shilu on 2015/7/7.
 */
@Repository
public interface ArticleDao {

    public Article getArticleById(Integer articleId);
}
