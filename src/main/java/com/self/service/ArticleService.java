package com.self.service;

import com.self.dao.ArticleDao;
import com.self.pojo.Article;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Created by shilu on 2015/7/7.
 */
@Service
public class ArticleService {

    @Autowired
    private ArticleDao articleDao;

    public Article getArticleById(Integer articleId){
        return articleDao.getArticleById(articleId);
    }
}
