package com.self.utils;

import org.dom4j.Document;
import org.dom4j.Element;
import org.dom4j.io.SAXReader;

import java.io.File;

/**
 * Created by shilu on 2015/7/8.
 */
public class VersionUtil {

    /**
     * 获取pom的版本号
     * @return
     * @throws Exception
     */
    public static String getPomVersion() throws Exception{

        // 创建saxReader对象
        SAXReader reader = new SAXReader();
        // 通过read方法读取一个文件 转换成Document对象
        Document document = reader.read(new File("pom.xml"));
        //获取根节点元素对象
        Element node = document.getRootElement();

        // 获取四大名著元素节点中，子节点名称为红楼梦元素节点。
        Element element = node.element("version");

        System.out.println(element.getText());
        return element.getText();
    }

    public static void main(String[] args) {
        try {
            getPomVersion();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
