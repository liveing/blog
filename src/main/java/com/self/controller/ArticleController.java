package com.self.controller;

import com.self.pojo.Article;
import com.self.service.ArticleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 * Created by shilu on 2015/7/7.
 */
@Controller
public class ArticleController {

    @Autowired
    private ArticleService articleService;

    @RequestMapping(value = "/index" , method = RequestMethod.GET )
    public String visitIndex(){
        return "/index";
    }

    @RequestMapping(value = "/about" , method = RequestMethod.GET )
    public String visitAbout(){
        return "/about";
    }

    @RequestMapping(value = "/caselist" , method = RequestMethod.GET )
    public String visitCaselist(){
        return "/caselist";
    }

    @RequestMapping(value = "/knowledge" , method = RequestMethod.GET )
    public String visitKnowledge(){
        return "/knowledge";
    }

    @RequestMapping(value = "/moodlist" , method = RequestMethod.GET )
    public String visitMoodlist(){
        return "/moodlist";
    }

    @RequestMapping(value = "/new" , method = RequestMethod.GET )
    public String visitNew(){
        return "/new";
    }

    @RequestMapping(value = "/newlist" , method = RequestMethod.GET )
    public String visitNewlist(){
        return "/newlist";
    }

    @RequestMapping(value = "/share" , method = RequestMethod.GET )
    public String visitShare(){
        return "/share";
    }

    @RequestMapping(value = "/template" , method = RequestMethod.GET )
    public String visitTemplate(){
        return "/template";
    }

    @RequestMapping(value = "/getArticleById/{articleId}" , method = RequestMethod.GET )
    public String getArticleById(@PathVariable Integer articleId,Model model){
        Article article = articleService.getArticleById(articleId);
        System.out.println("文章详情为："+article);
        model.addAttribute("article", article);
        return "/article";
    }

}
