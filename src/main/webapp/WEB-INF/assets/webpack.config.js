
var webpack = require('webpack');

var entry = {

  /*################订单模块################*/
  'article': [
      'webpack-dev-server/client?http://localhost:3000',
      'webpack/hot/only-dev-server',
    './js/article/app.js',
  ]
}


//production config
module.exports = {
  target: 'web',
  entry: entry,
  output: {
    path: __dirname.replace("assets","build").replace("WEB-INF","static"),
    filename: 'bundle-[name].js',
    publicPath: 'http://localhost:3000/static/build/'          //<script src="http://<A 的地址>/{publicPath}/bundle.js">
  },
  module: {
    loaders: [
      {test: /\.js/, loader: 'react-hot!babel', exclude: /node_modules/},
      {test: /\.css$/, loader: 'style-loader!css-loader' }
    ]
  },
  resolve: {
    modulesDirectories: [ 'js/base', 'web_modules', 'node_modules']
  },
  plugins: [
    new webpack.HotModuleReplacementPlugin(),
    new webpack.NoErrorsPlugin()
  ]
};